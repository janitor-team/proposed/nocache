nocache (1.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Dmitry Smirnov ]
  * New upstream release.
    + fixes hang with Glibc 2.28+ (Closes: #916415).
  * Debhelper & compat to version 11.
  * Standards-Version: 4.3.0.

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 04 Jan 2019 14:37:54 +1100

nocache (1.0-1) unstable; urgency=medium

  * New upstream release [May 2016].
  * Removed obsolete "make.patch".
  * Modernised Vcs URLs.
  * Updated get-orig-source.
  * Standards-Version: 3.9.8.
  * Build with full hardening.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 21 May 2016 14:13:28 +1000

nocache (0.9-2) unstable; urgency=medium

  * Disabled freezing post-build tests to fix FTBFS (Closes: #746888).
  * Standards-Version to 3.9.5.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 01 Jun 2014 19:53:43 +1000

nocache (0.9-1) unstable; urgency=low

  * New upstream release [May 2013].
    + dropped "man.patch" (applied-upstream).
  * New "make.patch" to rewrite Makefile from scratch in usable way.
    + simplified rules, let Makefile do the job.
  * No longer use our executable wrapper template (Closes: #710019).
  * Use NOCACHE_NR_FADVISE=2 for post-build tests to fix often failing
    test #3 and to test FADVISE>1.
  * get-orig-source to checkout tag and to set timestamp of generated
    ChangeLog to commit date.

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 31 May 2013 00:06:53 +1000

nocache (0.8+git20130430-1) unstable; urgency=low

  * New upstream release [April 2013].
  * Removed our man pages and ship upstream ones (our man pages were
    accepted and improved upstream).
  * New patch to fix "hyphen-used-as-minus-sign" in man pages.
  * get-orig-source improved to produce identical orig.tar (when
    possible) with accurate file's timestamps.
  * debian/watch to mangle debian version.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 02 May 2013 11:44:54 +1000

nocache (0+git20121017-2) unstable; urgency=low

  * Fixed Issue #5: extend (not override) LD_PRELOAD.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 18 Apr 2013 12:27:13 +1000

nocache (0+git20121017-1) unstable; urgency=low

  * Initial release (Closes: #702854).

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 18 Mar 2013 00:32:17 +1100
